//document refers to the HTML file linked to this JS file
//querySelector allows us to designate an HTML element that we want to access via its id or class name
//Use # for ids, and . for classes
let input1 = document.querySelector('#txt-first-name');
let span1 = document.querySelector('#span-full-name')

// Other ways to Access HTML elements
// document.egtElementById('#txt-first-name');
// document.egtElementsByClassName('#txt-inputs');
// document.getElementsByTagName('input')
// console.log(input1);

// console.dir(input1);
// console.log(input1.value);

// input1.value = "jack";
// console.log(span1.textContent); // ALl text, including whitespces
// console.log(span1.innerHTML); // All HTML and text within the given HTML element
// console.log(span1.innerText); // Just the TExt
// console.log(span1.outerText);

// Events and Event Listeners:
// An Event is ANY kind of user interaction with a specific element on a web page (CLicking a button, typing in an input, etc)

// Events have targets, which are the specific elements being interacted with

input1.addEventListener('keyup', (e) => {

	//console.log(input1.value);
	console.log(e.target.value);

});

/* ALtenate Veriosn
	
	input1.addEventListener('keyup', () => {

		//console.log(input1.value);
		console.log(event.target.value);

	});
*/



let button = document.getElementById('button');

// an event listener waits for a given event to occur before code execution
button.addEventListener('click', () => {
	alert("You CLicked it!");
});